# README #

### What is this repository for? ###

Version 1

Petite communication entre un serveur et un client en UDP
Le serveur est codé en GO
Le client est codé en GO ainsi qu'en Python!

### How do I get set up? ###

* Pour partir le serveur ainsi que le client GO : 
```go run server.go```
```go run client.go```


* Pour partir le client en Python :
```python client.py```

### Why !? ###

Pour le club CEDILLE, plus spécifiquement pour le projet de domotique où il faut activer des LED se trouvant sur une machine externe

[https://github.com/ClubCedille](Link URL)