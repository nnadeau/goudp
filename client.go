package main

import (
	"fmt"
	"net"
)

func CheckError(err error) {
	if err != nil {
		fmt.Println("Error: ", err)
	}
}

func main() {
	// Create server connection
	ServerAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:10001")
	CheckError(err)

	// Create local connection for come back packet
	LocalAddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:0")
	CheckError(err)

	// Dial up the to server
	Conn, err := net.DialUDP("udp", LocalAddr, ServerAddr)
	CheckError(err)

	// Create the message to be send to the server
	msg := "1"

	buf := []byte(msg)
	_, err = Conn.Write(buf)
	if err != nil {
		fmt.Println(msg, err)
	}

	// Ensure that the connection is close
	defer Conn.Close()
}
