package main
/*
* Name : server.go
* Author : Nicolas De Oliveira Nadeau
*	Description : small UDP server, listen on a specific port for a connection
* 							data and do what he have to do ! (LIGHTSHOW!)
* Next Step : GPIO, lib also working in go! so next step is to make it work
*/
import (
	"fmt"
	"net"
	"os"
)

/* A Simple function to verify error */
func CheckError(err error) {
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(0)
	}
}

/*
*	Main program !
* Server Side
*
 */
func main() {
	/* Lets prepare a address at any address at port 10001*/
	ServerAddr, err := net.ResolveUDPAddr("udp", ":10001")
	CheckError(err)

	/* Now listen at selected port */
	ServerConn, err := net.ListenUDP("udp", ServerAddr)
	CheckError(err)
	defer ServerConn.Close()

	buf := make([]byte, 1024)

	for {
		n, addr, err := ServerConn.ReadFromUDP(buf)
		fmt.Println("Received", string(buf[0:n]), "from", addr)

		if string(buf[0:n]) == "00" {
			fmt.Println("No face")
		}
		if string(buf[0:n]) == "01" {
			fmt.Println("Face")
		}
		if string(buf[0:n]) == "10" {
			fmt.Println("No eyes")
		}
		if string(buf[0:n]) == "11" {
			fmt.Println("Eyes")
		} else {
			fmt.Println("Don't know :/ : ", string(buf[0:n]))
		}

		if err != nil {
			fmt.Println("Error: ", err)
		}
	}
}
